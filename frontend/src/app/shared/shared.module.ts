import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataPipe } from './pipes/data.pipe';



@NgModule({
  declarations: [
    DataPipe
  ],
  exports:[
    DataPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
