import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { routes } from './app-routing.module';
import { CadastroRoutingModule } from '../app/Institutional/cadastro-usuario/cadastro-routing.module';
import { AppComponent } from './app.component';
import { CadastroModule } from './Institutional/cadastro-usuario/cadastro.module';
import { HeaderComponent } from './navegacao/header/header.component';
import { FooterComponent } from './navegacao/footer/footer.component';
import { LoginComponent } from './Institutional/login/login.component';
import { HomeComponent } from './Institutional/home/home.component';
import { SobreComponent } from './Institutional/sobre/sobre/sobre.component';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminModule } from './Institutional/admin/admin.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdminRoutingModule } from './Institutional/admin/admin-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    HomeComponent,
    SobreComponent,
  ],
  imports: [
    BrowserModule,
    CadastroModule,
    AdminModule,
    CadastroRoutingModule,
    SharedModule,
    AdminRoutingModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
