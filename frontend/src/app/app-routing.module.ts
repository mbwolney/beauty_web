import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroComponent } from './Institutional/cadastro-usuario/cadastro.component';
import { HomeComponent } from './Institutional/home/home.component';
import { LoginComponent } from './Institutional/login/login.component';
import { HeaderComponent } from './navegacao/header/header.component';
import { SobreComponent } from './Institutional/sobre/sobre/sobre.component';


export const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'sobre', component: SobreComponent},
  {path: 'header', component: HeaderComponent},
  {path: 'login', component: LoginComponent},
  {path: 'cadastro', component: CadastroComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
