import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AtualizarFuncionarioComponent } from './components/atualizar-funcionario/atualizar-funcionario.component';
import { AtualizarServicoComponent } from './components/atualizar-servico/atualizar-servico.component';
import { CadastroFuncionarioComponent } from './components/cadastro-funcionario/cadastro-funcionario.component';
import { CadastroServicoComponent } from './components/cadastro-servico/cadastro-servico.component';
import { ListagemAgendamentoComponent } from './components/listagem-agendamento/listagem-agendamento.component'
import { ListagemFuncionarioComponent } from './components/listagem-funcionario/listagem-funcionario.component'
import { ListagemServicoComponent } from './components/listagem-servico/listagem-servico.component';

export const AdminRoutes: Routes = [
    { path: 'admin', component: ListagemAgendamentoComponent },
    { path: 'cadastrofuncionario', component: CadastroFuncionarioComponent },
    { path: 'cadastroservico', component: CadastroServicoComponent },
    { path: 'listagemfuncionario', component: ListagemFuncionarioComponent },
    { path: 'listagemservico', component: ListagemServicoComponent },
    { path: 'atualizarservico/:servicoId', component: AtualizarServicoComponent },
    { path: 'listagemagendamento', component: ListagemAgendamentoComponent},
    { path: 'atualizarfuncionario/:funcionarioId', component: AtualizarFuncionarioComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(AdminRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AdminRoutingModule {

}