import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ListagemAgendamentoComponent } from './components/listagem-agendamento/listagem-agendamento.component';
import { ListagemFuncionarioComponent } from './components/listagem-funcionario/listagem-funcionario.component';
import { ListagemServicoComponent } from './components/listagem-servico/listagem-servico.component';
import { CadastroFuncionarioComponent } from './components/cadastro-funcionario/cadastro-funcionario.component';
import { CadastroServicoComponent } from './components/cadastro-servico/cadastro-servico.component';
import { ServicosService } from './services/servicos.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSortModule } from '@angular/material/sort'
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { AtualizarServicoComponent } from './components/atualizar-servico/atualizar-servico.component';
import { FuncionarioService } from './services/funcionario.service';
import { AgendamentoService } from './services/agendamento.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { AtualizarFuncionarioComponent } from './components/atualizar-funcionario/atualizar-funcionario.component';

@NgModule({
  declarations: [
    ListagemAgendamentoComponent,
    ListagemFuncionarioComponent,
    ListagemServicoComponent,
    CadastroFuncionarioComponent,
    AtualizarFuncionarioComponent,
    CadastroServicoComponent,
    AtualizarServicoComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatListModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatChipsModule,
    MatIconModule,
    SharedModule,
    MatSelectModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    RouterModule,
  ],
  providers: [
    ServicosService,
    FuncionarioService,
    AgendamentoService
  ]
})
export class AdminModule { }
