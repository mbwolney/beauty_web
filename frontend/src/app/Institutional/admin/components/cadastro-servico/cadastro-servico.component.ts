import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Servico } from '../../models/servico.model';
import { ServicosService } from '../../services/servicos.service';

@Component({
  selector: 'app-cadastro-servico',
  templateUrl: './cadastro-servico.component.html',
  styleUrls: ['./cadastro-servico.component.css']
})
export class CadastroServicoComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private service:ServicosService
  ) { }

  ngOnInit(): void {
    this.gerarForm();
  }

  gerarForm(){
    this.form = this.fb.group({
      nome: ['',[Validators.required]],
      valor:['',[Validators.required]]
    })
  }

  cadastrarServico(){
    const servico:Servico = this.form.value;
    this.service.cadastrarServico(servico)
    .subscribe(
      data=> {
        console.log(JSON.stringify(data));
          const msg: string = "Cadastro Realizado com Sucesso";
          alert(msg);
          this.router.navigate(['/listagemservico']);
        },
        err => {
          console.log(JSON.stringify(err));
          let msg: string = "Tente novamente em instantes";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
          alert(msg);
      }
    );
    return false;
  }

}
