import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Funcionario } from '../../models/funcionario.model';
import { FuncionarioService } from '../../services/funcionario.service';

@Component({
  selector: 'app-cadastro-funcionario',
  templateUrl: './cadastro-funcionario.component.html',
  styleUrls: ['./cadastro-funcionario.component.css']
})
export class CadastroFuncionarioComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private service: FuncionarioService
  ) { }

  ngOnInit(): void {
    this.gerarForm();
  }

  gerarForm(){
    this.form = this.fb.group({
      nome: ['',[Validators.required]]
    })
  }
  cadastrarFuncionario(){
    const funcionario:Funcionario = this.form.value;
    this.service.cadastrarFuncionario(funcionario)
    .subscribe(
      data=> {
        console.log(JSON.stringify(data));
          const msg: string = "Cadastro Realizado com Sucesso";
          alert(msg);
          this.router.navigate(['/listagemfuncionario']);
        },
        err => {
          console.log(JSON.stringify(err));
          let msg: string = "Tente novamente em instantes";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
          alert(msg);
      }
    );
    return false;
  }


}
