import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Funcionario } from '../../models/funcionario.model';
import { Servico } from '../../models/servico.model';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FuncionarioService } from '../../services/funcionario.service';
import { ServicosService } from '../../services/servicos.service';

@Component({
  selector: 'app-listagem-funcionario',
  templateUrl: './listagem-funcionario.component.html',
  styleUrls: ['./listagem-funcionario.component.css']
})
export class ListagemFuncionarioComponent implements OnInit {

  funcionarios: Funcionario[];
  servico: Servico[];

  constructor(
    private service: FuncionarioService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.listarFuncionario();
  }

  listarFuncionario() {
    this.service.listarFuncionario()
      .subscribe(
        data => {
          this.funcionarios = data;
        },
        err => {
          let msg: string = "Não foi possivel obter funcionários";
          this.router.navigate(['/home']);
          alert(msg);
        }
      )
  }
  removerFuncionario(funcionarioId: String) {
    this.service.removerFuncionario(funcionarioId)
      .subscribe(
        data => {
          this.listarFuncionario();
        },
        err => {
          let msg: string = "Tente novamento em instantes.";
          if (err.status == 400) {
            msg = "Não foi possivel Remover"
          } else
            if (err.status == 404) {
              msg = "Serviço não Localizado";
            }
          alert(msg);
        }
      )
  }
}
