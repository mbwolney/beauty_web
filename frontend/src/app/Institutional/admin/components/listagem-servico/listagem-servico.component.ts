import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servico } from '../../models/servico.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicosService } from '../../services/servicos.service';

@Component({
  selector: 'app-listagem-servico',
  templateUrl: './listagem-servico.component.html',
  styleUrls: ['./listagem-servico.component.css']
})
export class ListagemServicoComponent implements OnInit {

  servicos: Servico[];

  constructor(
    private service: ServicosService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.listarServico();
  }

  listarServico() {
    this.service.listarServico()
      .subscribe(
        data => {
          this.servicos = data
        },
        err => {
          let msg: string = "Não foi possivel obter serviços";
          this.router.navigate(['/home']);
          alert(msg);
        }
      );
  }

  removerServico(servicoId: String) {
    this.service.removerServico(servicoId)
      .subscribe(
        data => {
          this.listarServico();
        },
        err => {
          let msg: string = "Tente novamento em instantes.";
          if (err.status == 400){
            msg = "Não foi possivel Remover"
          }else 
          if (err.status == 404) {
            msg = "Serviço não Localizado";
          }
          alert(msg);
        }
      )
  }

}
