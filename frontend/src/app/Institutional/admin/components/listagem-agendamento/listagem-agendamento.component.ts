import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Agendamento } from '../../models/agendamento.model';
import { Funcionario } from '../../models/funcionario.model';
import { AgendamentoService } from '../../services/agendamento.service';
import { FuncionarioService } from '../../services/funcionario.service';
import { MatSelect, MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-listagem-agendamento',
  templateUrl: './listagem-agendamento.component.html',
  styleUrls: ['./listagem-agendamento.component.css']
})
export class ListagemAgendamentoComponent implements OnInit {

  funcionarioId: string;
  agendamentos: Agendamento[];
  funcionarios: Funcionario[];
  @ViewChild(MatSelect) matSelect: MatSelect;
  form: FormGroup;

  constructor(
    private service: AgendamentoService,
    private fb: FormBuilder,
    private serviceFuncionario: FuncionarioService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.obterFuncionario();
    this.gerarForm();
    this.listarAgendamento();
  }

  gerarForm() {
    this.form = this.fb.group({
      funcs: ['', []]
    });
  }

  get funcId(): string {
    return sessionStorage['funcionarioId'] || false;
  }

  obterFuncionario() {
    this.serviceFuncionario.listarFuncionario().subscribe(
      data => {
        this.funcionarios = data
      }
    )
  }

  listarAgendamento() {
    this.service.listarAgendamento().subscribe(
      data => {
        this.agendamentos = data;
        console.log(this.matSelect.selected['value']);
      },
      err => {
        let msg: string = "Não foi possivel obter agendamentos";
        this.router.navigate(['/home']);
        alert(msg);
      }
    )
  }

  listarPorFuncionario() {
    this.service.buscarPorFuncionario(this.matSelect.selected['value']).subscribe(
      data => {
        this.agendamentos = data;
      }
    )
  }

  onSearch() {
    if (this.matSelect.selected) {
      this.funcionarioId = this.matSelect.selected['value'];
    } else if (this.funcId) {
      this.funcionarioId = this.funcId;
    } else {
      return;
    }
    sessionStorage['funcionarioId'] = this.funcionarioId;
    this.service.buscarPorFuncionario(this.funcionarioId).subscribe(
      data => {
        this.agendamentos = data;
        console.log(this.agendamentos);
      },
      err => {
        this.listarAgendamento();
      }
    )
  }

}
