import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Servico } from '../../models/servico.model';
import { ServicosService } from '../../services/servicos.service';


@Component({
  selector: 'app-atualizar-servico',
  templateUrl: './atualizar-servico.component.html',
  styleUrls: ['./atualizar-servico.component.css']
})
export class AtualizarServicoComponent implements OnInit {

  form: FormGroup;
  servicoId: String;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private service: ServicosService
  ) { }

  ngOnInit() {
    this.servicoId = this.route.snapshot.paramMap.get('servicoId');
    this.gerarForm();
    this.obterDadosServico();
  }
  obterDadosServico() {
    this.service.buscarPorId(this.servicoId)
      .subscribe(
        dados => {
          const data = dados.data;
          this.form.get('id').setValue(data.id);
          this.form.get('nome').setValue(data.nome);
          this.form.get('valor').setValue(data.valor);
        },
        err => {
          let msg: string = "Error ao obter Serviço"
          this.router.navigate(['/listagemservico'])
        }
      );
  }

  atualizarServico() {
    const servico: Servico = this.form.value;
    this.service.atualizarServico(servico)
      .subscribe(
        data => {
          console.log(JSON.stringify(data));
          const msg: string = "Serviço atualizado com Sucesso";
          alert(msg);
          this.router.navigate(['/listagemservico']);
        },
        err => {
          console.log(JSON.stringify(err));
          let msg: string = "Tente novamente em instantes";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
          alert(msg);
        }
      );
    return false;
  }
  gerarForm() {
    this.form = this.fb.group({
      id:['', [Validators.required]],
      nome: ['', [Validators.required]],
      valor: ['', [Validators.required]]
    })
  }

}
