import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Funcionario } from '../models/funcionario.model';
import { environment as env } from '../../../../environments/environment';

@Injectable()
export class FuncionarioService {

  private readonly PATH: string = 'funcionario';
  constructor(private http: HttpClient) { }

  listarFuncionario(): Observable<any>{
    return this.http.get<Funcionario[]>(env.baseUrl + this.PATH);
  }
  cadastrarFuncionario(cadastro: Funcionario): Observable<any> {
    return this.http.post(env.baseUrl + this.PATH, cadastro);
  }
  removerFuncionario(funcionarioId: String): Observable<any> {
    return this.http.delete(
      env.baseUrl + this.PATH + '/' + funcionarioId
    );
  }
}
