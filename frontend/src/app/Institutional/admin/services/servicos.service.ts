import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';
import { Servico } from '../models/servico.model';

@Injectable()
export class ServicosService {

  private readonly PATH: string = 'servico';

  constructor(private http: HttpClient) { }

  listarServico(): Observable<any> {
    return this.http.get<Servico[]>(env.baseUrl + this.PATH);
  }
  buscarPorId(servicoId: String): Observable<any> {
    return this.http.get(
      env.baseUrl + this.PATH + '/' + servicoId
    );
  }
  atualizarServico(cadastro: Servico): Observable<any>{
    return this.http.put(
      env.baseUrl + this.PATH + '/' + cadastro.id, cadastro
    );
  }
  cadastrarServico(cadastro: Servico): Observable<any> {
    return this.http.post(env.baseUrl + this.PATH, cadastro);
  }
  removerServico(servicoId: String): Observable<any> {
    return this.http.delete(
      env.baseUrl + this.PATH + '/' + servicoId
    );
  }

}
