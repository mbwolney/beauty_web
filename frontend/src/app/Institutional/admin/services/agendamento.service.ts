import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Agendamento } from '../models/agendamento.model';
import { environment as env } from '../../../../environments/environment';

@Injectable()
export class AgendamentoService {

  private readonly PATH: string = 'agendamento'
  constructor(private http: HttpClient) { }

  listarAgendamento():Observable<any>{
    return this.http.get<Agendamento[]>(env.baseUrl + this.PATH);
  }

  buscarPorFuncionario(funcionarioId: String): Observable<any>{
    return this.http.get(env.baseUrl + this.PATH + '/' + funcionarioId)
  }
}
