import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroComponent } from './cadastro.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { CadastroRoutingModule } from './cadastro-routing.module';
import { CadastroService } from './services/cadastro.service';

@NgModule({
  declarations: [
    CadastroComponent,
  ],
  imports: [ 
    CommonModule,
    HttpClientModule, 
    ReactiveFormsModule,
    CadastroRoutingModule,
    RouterModule,
  ],
  providers: [
    CadastroService,
  ]
})
export class CadastroModule { 
  
}
