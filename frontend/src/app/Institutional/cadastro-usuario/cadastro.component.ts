import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Cadastro } from './cadastro.model';
import { CadastroService } from './services/cadastro.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private cadastroService: CadastroService
  ) { }

  ngOnInit(): void {
    this.gerarForm();
  }

  gerarForm() {
    this.form = this.fb.group({
      nome: ['', [Validators.required]],
      email: ['', [Validators.required]],
      cpf: ['', [Validators.required]],
      senha: ['', [Validators.required]]
    })
  }

  cadastrarUsuario() {
    const cadastro: Cadastro = this.form.value;
    this.cadastroService.cadastrarUsuario(cadastro)
      .subscribe(
        data => {
          console.log(JSON.stringify(data));
          const msg: string = "Cadastro Realizado com Sucesso";
          alert(msg);
          this.router.navigate(['/login']);
        },
        err => {
          console.log(JSON.stringify(err));
          let msg: string = "Tente novamente em instantes";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
          alert(msg);
        }
      );
    return false;
  }

}


