import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CadastroComponent} from './cadastro.component';

const CasdastroRoutes: Routes = [{
    path: 'cadastro', 
    component: CadastroComponent
}];

@NgModule({
    imports: [RouterModule.forChild(CasdastroRoutes)],
    exports: [RouterModule]
})

export class CadastroRoutingModule{

}
