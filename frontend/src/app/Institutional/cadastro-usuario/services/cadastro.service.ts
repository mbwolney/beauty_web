import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Cadastro } from '../cadastro.model';
import { environment as env } from '../../../../environments/environment';

@Injectable()
export class CadastroService {

  private readonly PATH: string = 'usuario';

  constructor(private http: HttpClient) { }

  cadastrarUsuario(cadastro: Cadastro): Observable<any> {
    return this.http.post(env.baseUrl + this.PATH, cadastro);
  }
}
