package com.example.beauty.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.OpInc;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.beauty.entity.Usuario;
import com.example.beauty.service.UsuarioService;


@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioService usuarioDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> usu = usuarioDao.findbyEmail(username);

		if (usu.isPresent()) {
			return JwtUserFactory.create(usu.get());
		}

		throw new UsernameNotFoundException("Email não encontrado");
	}

}
